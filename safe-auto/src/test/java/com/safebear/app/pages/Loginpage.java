package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 30/08/2017.
 */
public class Loginpage {
    WebDriver driver;
    @FindBy(id="myid")
    WebElement username;
    @FindBy(id="mypass")
    WebElement password;

    public Loginpage(WebDriver driver)
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);

    }

    public boolean checkCorrectPage()
    {
        return driver.getTitle().startsWith("Sign In");
    }


    public boolean login(UserPage userpage, String username,String password)
    {
        this.username.sendKeys(username);
        this.password.sendKeys(password);
        this.password.submit();
        return userpage.checkCorrectPage();
    }

}

