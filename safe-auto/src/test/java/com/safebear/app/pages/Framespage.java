package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 30/08/2017.
 */
public class Framespage {
    WebDriver driver;
    public Framespage(WebDriver driver)
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);

    }

    public boolean checkCorrectPage()
    {
        return driver.getTitle().startsWith("Frame Page");
    }}
